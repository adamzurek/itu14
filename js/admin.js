$(document).ready(function() {

    // priklady galerie: http://darsa.in/sly/examples/horizontal.html

    var $wrap   = $('#gallery');
    var $frame  = $wrap.find('.frame');
    var $slidee = $frame.children('ul').eq(0);

    // Call Sly on frame
    $frame.sly({
        horizontal: 1,
        itemNav: 'basic',
        smart: 1,
        activateOn: 'click',
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        startAt: 3,
        scrollBar: $wrap.find('.scrollbar'),
        scrollBy: 1,
        pagesBar: $wrap.find('.pages'),
        activatePageOn: 'click',
        speed: 300,
        elasticBounds: 1,
        easing: 'easeOutExpo',
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1
    });

    var $main_menu = $('#main-menu');

    // zobrazeni napovedy
    var $help = $('#help');
    $main_menu.find('.help').on('click', function(e) {

        if ($help.hasClass('active')) {
            $help.removeClass('active');
        } else {
            $help.find('#help-content').load(this.href, function () {
                $help.addClass('active');
            });
        }

        e.preventDefault();
    });

    // zavreni napovedy
    $help.find('.close').on('click', function(e) {
        $help.removeClass('active');

        e.preventDefault();
    });

    // slajdovani na kotvy z menu
    $main_menu.find('a[href^="#"]').on('click', function(e) {
        var id = $(this).attr('href').substr(1);

        $('body,html').animate({'scrollTop': $('#' + id).offset().top - $('.navbar-fixed-top').first().height()}, 1000, 'easeOutBack');
        location.hash = id;

        e.preventDefault();
    });


    // změna velikosti náhledu
    var $thumb = $('.thumbnail');
    var $glyph = $thumb.find('.glyphicon');

    $('input[name="width"]').on('change', function() {
        $thumb.css('width', $(this).val());
        $glyph.css('font-size', ($thumb.css('width')*100/$thumb.css('height')) + 'px');
    });

    $('input[name="height"]').on('change', function() {
        $thumb.css('height', $(this).val());
        $glyph.css('font-size', ($thumb.css('width')*100/$thumb.css('height')) + 'px');
    });
    $('input[name="width"], input[name="height"]').trigger('change');

    // změna stylu zaškrtnutých fotek
    $thumb.find(':checkbox').on('change', function() {
        if ($(this).is(':checked')) {
            $(this).parent().css('background-color', 'rgba(0,0,0,0.5)');
        } else {
            $(this).parent().css('background-color', 'rgba(255,255,255, 1)');
        }
    });
    $thumb.find(':checkbox').trigger('change');

    /**  akce s albem **/
    var $album = $('#album');

    // přidání alba
    $('#albumadd').on('click', function() {
        bootbox.prompt('Zadejte nový název alba', function(result) {
            if (result != null)
            {
                $album.append('<option>' + result + '</option>');
            }
        });
    });

    // odebrání alba
    $('#albumremove').on('click', function() {
       if ($album.find(':selected').length > 0) {
           $album.find(':selected').remove();
           $album.trigger('change');
       }
    });

    // při změně alba vyhodit starou galerii a dát novou
    $album.on('change', function() {
        $('.frame').fadeOut(1000, function() {
           $(this).slideDown(2000);
            $thumb.find(':checked').prop('checked', false);
            $thumb.find(':checkbox').trigger('change');
        });

    });

    // přejmenování alba
    $('#rename').on('click', function() {
        var $selectedAlbum = $album.find(':selected');

        if ($selectedAlbum.length == 0) {
            bootbox.alert('Zvolte nějaké album.');
            return;
        }

        bootbox.prompt('Zadejte nový název pro album ' + $selectedAlbum.text(), function(result) {
           if (result != null) {
               $selectedAlbum.text(result);
           }
        });
    });

    // informace o albu
    $('#info').on('click', function() {
        bootbox.alert('Název: ' + $album.find(':selected').text() + '<br>Počet fotek: ' + $thumb.length);
    });

    // přidat foto
    $('#add').on('click', function() {
       $thumb.clone().appendTo($thumb.parent());
    });

    // odebrat foto
    $('#remove').on('click', function() {
       $thumb.find(':checked').parent().remove();
    });

    // info o fotu
    $('#photoinfo').on('click', function() {
       $thumb.find(':checked ~ .caption').each(function(i, el) {
          bootbox.alert('Název: ' + $(el).text());
       });
    });

    // otočit doleva
    $('#rotateleft').on('click', function() {
        $thumb.find(':checked ~ .glyphicon').css('transform', function(i) {
            return 'rotate(' + (getRotationDegrees($(this)) - 90) + 'deg)';
        });
    });

    // otočit doprava
    $('#rotateright').on('click', function() {
        $thumb.find(':checked ~ .glyphicon').css('transform', function(i) {
            return 'rotate(' + (getRotationDegrees($(this)) + 90) + 'deg)';
        });
    });
});

/**
 * Funkce na zjištění aktuálního natočení obrázku (přes css transform)
 *
 * @param obj
 * @returns {number}
 */
function getRotationDegrees(obj) {
    var angle = 0;

    var matrix = obj.css("-webkit-transform") ||
        obj.css("-moz-transform")    ||
        obj.css("-ms-transform")     ||
        obj.css("-o-transform")      ||
        obj.css("transform");
    if(matrix !== 'none') {
        var values = matrix.split('(')[1].split(')')[0].split(',');
        var a = values[0];
        var b = values[1];
        angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
    }

    return angle;
}
