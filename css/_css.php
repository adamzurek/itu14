<?php
require_once '../_lib/less.php/Less.php';

try {
    header('content-type: text/css');

	$options = array('cache_dir' => __DIR__ . '/cache');
	if (isset($_GET['admin'])) {
		$files = array('../admin/css/style.less' => '/');
	} else {
    	$files = array('style.less' => '');
	}
	
    echo file_get_contents($options['cache_dir'] . '/' . Less_Cache::Get($files, $options));
} catch(Exception $e) {
    trigger_error($e->getMessage(), E_USER_ERROR);
}
